import React from "react";
import { Table } from "reactstrap";
import styled from 'styled-components';


const ClicableTh = styled.th`
    text-align: center; 
    cursor: pointer;
    :hover {
        text-decoration: underline;
        color: #97A2FF;
    }
`;

const AlignTd = styled.td`
    text-align: center;
    width: 75px;
    height: 30px;
    vertical-align: middle;
`;
const AlignId = styled.td`
    text-align: center;
    width: 20px;
    height: 30px;
`;

const AlignFoto = styled.img`
    text-align: center;
    height: 65px;
    width: 65px;
`;


export default (props) => {

    const srcerror= 'https://vignette.wikia.nocookie.net/rickandmorty/images/4/49/Antenna_Rick.png/revision/latest?cb=20161121231006'

    let errors = [19]

    const filas = props.datos.map((el) => (
        <tr key={el.id}>
            <AlignId>{el.id}</AlignId>
            <AlignTd>{el.name}</AlignTd>
            <AlignTd>{el.gender}</AlignTd>
            <AlignTd>{el.status}</AlignTd>
            <AlignTd>{el.species}</AlignTd>
            <AlignTd>{el.origin.name}</AlignTd>
            <AlignTd><AlignFoto src={(errors.includes(el.id)) ? srcerror : el.image}></AlignFoto></AlignTd>
        </tr>
    ));

    return (
        <Table dark>
            <thead>
                <tr>
                    <ClicableTh>#</ClicableTh>
                    <ClicableTh>Nombre</ClicableTh>
                    <ClicableTh>Género</ClicableTh>
                    <ClicableTh>Estado</ClicableTh>
                    <ClicableTh>Especie</ClicableTh>
                    <ClicableTh>Origen</ClicableTh>
                    <ClicableTh>Foto</ClicableTh>
                </tr>

            </thead>
            <tbody>{filas}</tbody>
            <tfoot></tfoot>
        </Table>
    );
};
