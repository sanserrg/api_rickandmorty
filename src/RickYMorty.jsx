import React, { useState, useEffect } from 'react';
import { Container } from 'reactstrap';
import TaulaApi from './TaulaApi.jsx';


export default () => {

    const [personajes, setPersonajes] = useState([]);
    const [page, setPage] = useState(1);

    const siguiente = () => {
        setPage(page + 1)
    }

    useEffect(() => {

        fetch(`https://rickandmortyapi.com/api/character?page=${page}`)
            .then(dades => dades.json())
            .then(dades => setPersonajes(dades.results))
            .catch(err => console.log(err));
    }, [page])

    return (
        <>
            <Container>
                <h1>Rick y Morty</h1>
                <button onClick={siguiente}>Siguiente</button>
                <TaulaApi datos={personajes} />
            </Container>
        </>
    );
}

